﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataloggerServices.Models
{
    public class SimpleDataLogEntryMap
    {
        public SimpleDataLogEntryMap(EntityTypeBuilder<SimpleDataLogEntry> entity)
        {          
            entity.Property(e => e.Id);
            entity.Property(e => e.DeviceId);           
            entity.Property(e => e.DeviceTimeStamp);
            entity.Property(e => e.Temperature);
            entity.Property(e => e.Humidity);
            entity.Property(e => e.Co2);
            entity.Property(e => e.VOC);
            entity.Property(e => e.Noise);
            
            

        }
    }
}
