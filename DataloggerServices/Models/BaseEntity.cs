﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataloggerServices.Models
{
    public class BaseEntity : IBaseEntity, IDisposable
    {
        public int Id { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
