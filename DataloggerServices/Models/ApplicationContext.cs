﻿using Microsoft.EntityFrameworkCore;

namespace DataloggerServices.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new SimpleDataLogEntryMap(modelBuilder.Entity<SimpleDataLogEntry>());
        }
    }
}
