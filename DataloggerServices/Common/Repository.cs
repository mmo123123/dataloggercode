﻿using DataloggerServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DataloggerServices.Common
{
    public class Repository<TEntity> : IRepository<TEntity>
    where TEntity : BaseEntity
    {
        private ApplicationContext _context;
        private DbSet<TEntity> _entity;

        public Repository(ApplicationContext context)
        {
            _context = context;
            _entity = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _entity.AsNoTracking();
        }

        public TEntity GetById(long id)
        {
            return _entity.FirstOrDefault(s => s.Id == id);
        }

        public TEntity Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }
            var createdEntity = _entity.Add(entity);
            _context.SaveChanges();
            return createdEntity.Entity;
        }

        

        public IQueryable<TEntity> DoSQL(string sql)
        {
            return _entity.FromSql(sql);
        }

        public int BulkOperation(string sql)
        {
            return _context.Database.ExecuteSqlCommand(sql);
        }

        public void Dispose()
        {

        }
    }
}
