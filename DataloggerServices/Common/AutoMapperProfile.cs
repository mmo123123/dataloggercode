﻿using AutoMapper;
using DataloggerServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataloggerServices.Common
{
    public class AutomapperProfile : Profile
    {
        private IRepository<SimpleDataLogEntry> repo;
        public AutomapperProfile(IRepository<SimpleDataLogEntry> repository)
        {
            repo = repository;

            //CreateMap<SimpleDataLogEntry, DataLogEntry>()
            //        .ForMember(dest => dest.Name, opts => opts.MapFrom(x => x.Name))
            //        .ForMember(dest => dest.Id, opts => opts.MapFrom(x => x.Id))
            //        .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.Value));

            //CreateMap<DataLogEntry, SimpleDataLogEntry>()
            //        .ForMember(dest => dest.Name, opts => opts.MapFrom(x => x.Name))                
            //        .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.Value));
        }

        private SimpleDataLogEntry getAll()
        {
            
            return (SimpleDataLogEntry) repo.GetAll();
        }

    }
}
