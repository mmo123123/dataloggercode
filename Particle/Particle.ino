//!!
//Please note to use this code go to particle.io and create an account and paste this code into web ide and upload to particle device.
//!!


// This #include statement was automatically added by the Particle IDE.
#include <HttpClient.h>


int photoresistor = D1;
int led1 = D0;
int led2 = D7;
int intervalIOTPirMessage = 30000;
unsigned long previousMillisPirCheck = 0;
int pir_Detected = 0;
int pir = 0;

#define WEBSITE      "dataloggerservicesazurepoc.azurewebsites.net"
#define WEBPAGE      "/api/DataLogEntry/"

unsigned int nextTime = 0;    // Next time to contact the server
HttpClient http;

// Headers currently need to be set at init, useful for API keys etc.
http_header_t headers[] = {
	//  { "Content-Type", "application/json" },
	//  { "Accept" , "application/json" },
	{ "Accept" , "*/*"},
	{ NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;


void setPirHighIfDetectedInWaitingTimeTosend()
{
	pir = digitalRead(photoresistor);
	if (pir == 1)
	{
		pir_Detected = 1;
	}

}

void setup() {
	pinMode(photoresistor, INPUT);
	pinMode(led1, OUTPUT);
	pinMode(led2, OUTPUT);
}


void loop()
{

	unsigned long currentMillis = millis();

	setPirHighIfDetectedInWaitingTimeTosend();

	//Particle.publish(i.ToString());
	if (pir > 0)
	{
		digitalWrite(led2, HIGH);
	}
	else
	{
		digitalWrite(led2, LOW);
	}

	//http://dataloggerservicesazurepoc.azurewebsites.net/api/DataLogEntry/5,5,5,5,5,5,1




	if ((unsigned long)(currentMillis - previousMillisPirCheck) >= intervalIOTPirMessage) {

		Particle.publish(String(currentMillis));
		Particle.publish(String(previousMillisPirCheck));
		Particle.publish(String(intervalIOTPirMessage));
		/*works*/
		request.hostname = "dataloggerservicesazurepoc.azurewebsites.net";
		request.port = 80;
		char path[128];
		snprintf(path, 128, "/api/DataLogEntry/Particle01,-9999999,-9999999,-9999999,-9999999," + String(pir_Detected) + ",-9999999");
		request.path = path;
		http.get(request, response, headers);

		if (response.status == 200) {
			//Particle.publish("/api/DataLogEntry/Particle01,-9999999,-9999999,-9999999,-9999999,"+String(pir)+",-9999999");
		}
		else {
			Particle.publish("photon", String::format("Response.body: %s startup: %.3f Response.status: %d ID: %s", (const char*)response.body, "startup", response.status, "(const char*)deviceID)"));
			// Particle.publish("/api/DataLogEntry/Particle01,-9999999,-9999999,-9999999,-9999999,"+String(pir)+",-9999999");
		}


		previousMillisPirCheck = currentMillis;
		pir_Detected = 0;
		pir = 0;

		
		digitalWrite(led1, HIGH);
		delay(500);
		digitalWrite(led1, LOW);
	}


	/*digitalWrite(led2,HIGH);
	delay(5000);
	digitalWrite(led2,LOW);
	delay(5000);*/


}